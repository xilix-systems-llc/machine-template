export const set = (property: string | number) => (state: any, payload: any) =>
  (state[property] = payload);

export const toggle = (property: string | number) => (state: any) =>
  (state[property] = !state[property]);
