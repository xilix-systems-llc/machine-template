import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/views/Dashboard.vue';
import Overview from '@/views/Overview.vue';
import Status from '@/views/Status.vue';
import TestSettings from '@/views/TestSettings.vue';
import Alarms from '@/views/Alarms.vue';
import Print from '@/views/Print.vue';
import AlarmHistory from '@/views/AlarmHistory.vue';

Vue.use(Router);

export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '/overview',
      name: 'overview',
      component: Overview,
    },
    {
      path: '/status',
      name: 'status',
      component: Status,
    },
    {
      path: '/test-settings',
      name: 'test settings',
      component: TestSettings,
    },
    {
      path: '/alarms',
      name: 'alarms',
      component: Alarms,
    },
    {
      path: '/alarm-history',
      name: 'alarm history',
      component: AlarmHistory,
    },
    {
      path: '/print',
      name: 'print',
      meta: { layout: 'nocruft' },
      component: Print,
    },
  ],
});
