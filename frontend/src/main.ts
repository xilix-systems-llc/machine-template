import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
import { convertPascalToSpace } from './store/helpers';
import VueVirtualScroller from 'vue-virtual-scroller';

Vue.use(VueVirtualScroller)

Vue.config.productionTip = false;

Vue.filter('toFixed', function(value: string) {
  if (!value) return '';
  value = parseFloat(value).toFixed(3);
  return value;
});

Vue.filter('toSpaced', function(value: string) {
  if (!value) return '';
  value = convertPascalToSpace(value);
  return value;
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
