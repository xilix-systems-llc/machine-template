// Type definitions for Chart.js 2.8
// Project: https://github.com/nnnick/Chart.js, https://www.chartjs.org
// Definitions by: Bob Klosinski <https://github.com/fluxin>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.3

declare class uPlot {
  static readonly uPlot: typeof uPlot;
  constructor(options: Chart.ChartOptions, series: Chart.ChartConfiguration);
  setData: (Chart.ChartData): void;

  static defaults: {
    global: Chart.ChartOptions & Chart.ChartFontOptions;
    [key: string]: any;
  };
}

declare namespace Chart {
  interface ChartData{ 
    Array<number | string[]> };

  interface ChartOptions {
    responsive?: boolean;
    responsiveAnimationDuration?: number;
    aspectRatio?: number;
    maintainAspectRatio?: boolean;
    events?: string[];
    legendCallback?(chart: Chart): string;
    onHover?(this: Chart, event: MouseEvent, activeElements: Array<{}>): any;
    onClick?(event?: MouseEvent, activeElements?: Array<{}>): any;
    onResize?(this: Chart, newSize: ChartSize): void;
    title?: ChartTitleOptions;
    legend?: ChartLegendOptions;
    tooltips?: ChartTooltipOptions;
    hover?: ChartHoverOptions;
    animation?: ChartAnimationOptions;
    elements?: ChartElementsOptions;
    layout?: ChartLayoutOptions;
    scale?: RadialLinearScale;
    scales?: ChartScales;
    showLines?: boolean;
    spanGaps?: boolean;
    cutoutPercentage?: number;
    circumference?: number;
    rotation?: number;
    devicePixelRatio?: number;
    plugins?: ChartPluginsOptions;
  }

  interface CommonAxe {
    bounds?: string;
    type?: ScaleType | string;
    display?: boolean;
    id?: string;
    stacked?: boolean;
    position?: string;
    ticks?: TickOptions;
    gridLines?: GridLineOptions;
    barThickness?: number | 'flex';
    maxBarThickness?: number;
    minBarLength?: number;
    scaleLabel?: ScaleTitleOptions;
    time?: TimeScale;
    offset?: boolean;
  }

  interface ChartXAxe extends CommonAxe {
    categoryPercentage?: number;
    barPercentage?: number;
    distribution?: 'linear' | 'series';
  }

  // tslint:disable-next-line no-empty-interface
  interface ChartYAxe extends CommonAxe {}

}
export = Chart;
export as namespace Chart;
