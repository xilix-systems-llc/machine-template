import { IHash } from '@/services/variableDeclarations';

export interface TestSettingsType {
  operator: string;
  serialNumber: string;
}

export interface RootState {
  plc: IHash;
  alarms: IHash;
  status: IHash;
  pauseChart: boolean;
  chartPoints: Array<{ x: number; y: number }>;
  testSettings: TestSettingsType;
  updatingChart: boolean;
}
