import Vue from 'vue';
import Vuex, { MutationTree, ActionTree, GetterTree } from 'vuex';
import { objectByString, stringToBoolOrValue } from '@/store/helpers';
import { IHash } from '@/services/variableDeclarations';
import { RootState, TestSettingsType } from '@/store/types';
import PLC from '@/services/plc';
import appModule from './app';

Vue.use(Vuex);
const plc = new PLC();

const state: RootState = {
  plc: {},
  alarms: {},
  status: {},
  pauseChart: false,
  chartPoints: [{ x: 0, y: 0 }],
  testSettings: {} as TestSettingsType,
  updatingChart: false,
};

const getters: GetterTree<RootState, any> = {
  Chart(localState) {
    return localState.chartPoints;
  },
  ChartMin(localState) {
    return localState.chartPoints.reduce((prev, curr) =>
      prev.x < curr.x ? prev : curr
    ).x;
  },
  ChartMax(localState) {
    return localState.chartPoints.reduce((prev, curr) =>
      prev.x > curr.x ? prev : curr
    ).x;
  },
};
const mutations: MutationTree<RootState> = {
  PLC_UPDATED(localState: RootState, payload) {
    function changeValue(
      functionState: any,
      variable: string,
      data: IHash | string | boolean
    ) {
      if (typeof data === 'object') {
        for (const [key, value] of Object.entries(data)) {
          const curObj = objectByString(functionState, variable);
          const nextObj = objectByString(functionState, `${variable}.${key}`);
          if (key === 'Chart') {
            debugger;
          }
          if (nextObj === undefined) {
            try {
              Vue.set(curObj, key, stringToBoolOrValue(value));
            } catch {
              debugger;
            }
          }
          changeValue(curObj, key, stringToBoolOrValue(value));
        }
      } else {
        Vue.set(functionState, variable, stringToBoolOrValue(data as string));
      }
    }
    changeValue(localState, payload.variable, payload.data);
  },
  SET_PAUSE_CHART(localState, pause) {
    localState.pauseChart = pause;
  },
  SET_CHART(localState, chartPoints) {
    localState.updatingChart = true;
    localState.chartPoints = chartPoints;
    localState.updatingChart = false;
  },
  UPDATE_TEST_SETTINGS(localState, payload: TestSettingsType) {
    localState.testSettings = { ...payload };
  },
};

const actions: ActionTree<RootState, any> = {
  UPDATE_PLC(localState: any, payload: any) {
    plc.updatePLC(payload.variable, payload.value);
  },
  REQUEST_UPDATE(localState: any, payload: any) {
    payload.forEach((element: any) => {
      plc.requestVariableUpdate(element);
    });
  },
  async getChartUpdate({ commit }, { width = 640 }) {
    const points = await plc.getPoints(width);
    if (points === undefined) {
      return;
    }
    // commit('SET_CHART', points.Value);
  },
  togglePauseChart({ commit, state: localState }) {
    commit('SET_PAUSE_CHART', !localState.pauseChart);
  },
  updateChartData({ commit, state: localState }, data) {
    if (localState.updatingChart) {
      console.log('skipped');
      return;
    }
    commit('SET_CHART', data);
  },
};

const store = new Vuex.Store<RootState>({
  state,
  mutations,
  actions,
  getters,
  modules: {
    app: appModule,
  },
  plugins: [plc.plc],
});

export default store;
