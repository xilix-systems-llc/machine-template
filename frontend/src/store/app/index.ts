import { Module } from 'vuex';
import { set, toggle } from '@/utils/vuex';
import { RootState } from '../types';

export interface AppSate {
  drawer: boolean;
}

const namespaced: boolean = true;
export const appModule: Module<AppSate, RootState> = {
  namespaced,
  state: {
    drawer: true,
  },
  getters: {},
  mutations: {
    setDrawer: set('drawer'),
    toggleDrawer: toggle('drawer'),
  },
  actions: {},
};
export default appModule;
