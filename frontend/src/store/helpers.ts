import { IHash } from '@/services/variableDeclarations';

export function objectByString(o: any, s: string) {
  // s = s.replace(/\[(\w+)\]/g, '.$1') // convert indexes to properties
  s = s.replace(/^\./, ''); // strip a leading dot
  const a = s.split('.');
  for (let i = 0, n = a.length; i < n; ++i) {
    const k = a[i];
    if (k in o) {
      if (k.startsWith('[')) {
        o = o[`"${k}"`];
      } else {
        o = o[k];
      }
    } else {
      return;
    }
  }
  return o;
}

export function getKeyByValue(object: any, value: string) {
  return Object.keys(object).find(key => object[key] === value);
}

export function stringToBoolOrValue(value: string): boolean | string | IHash {
  if (value.toString().toLowerCase() === 'false') return false;
  if (value.toString().toLowerCase() === 'true') return true;
  return value;
}

export function convertPascalToSpace(value: string): string {
  const result = value.replace(/([A-Z])/g, ' $1');
  const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
  return finalResult;
}
