import { Store, Plugin as vuexPlugin } from 'vuex';
import ky from 'ky';
import { RootState } from '@/store/types';
import vars from './variableDeclarations';
import { getKeyByValue } from '@/store/helpers';
import { stringToBoolOrValue } from '@/store/helpers';

interface PlcCommand {
  command: commandType;
  variable: string;
  value: string;
}

enum commandType {
  UPDATE = 1,
  GET,
}

export default class PLC {
  private store?: Store<RootState>;

  private pause: boolean = false;

  private commandCache: PlcCommand[] = [];

  private inSend: boolean = false;

  public plc: vuexPlugin<RootState> = (store: Store<RootState>) => {
    this.store = store;
    this.setupEvents(store);
  };

  private setupEvents(store: Store<RootState>) {
    try {
      const eventSource = new EventSource('/events');
      eventSource.onmessage = event => {
        this.receiveMessage(event, store);
      };
      this.sendCommand(store);
    } catch {
      this.setupEvents(store);
    }
  }

  public async sendCommand(localStore: Store<RootState>) {
    if (this.inSend) return;
    while (this.commandCache.length > 0) {
      this.inSend = true;
      const nextCommand = this.commandCache.pop() as PlcCommand;
      switch (nextCommand.command) {
        case commandType.UPDATE: {
          try {
            await ky.put(`/api/plc/${nextCommand.variable}`, {
              body: nextCommand.value,
            });
          } catch {
            this.commandCache.push(nextCommand);
          }
          break;
        }
        case commandType.GET: {
          try {
            const response = await ky
              .get(`/api/plc/${nextCommand.variable}`)
              .json();
            const key = getKeyByValue(vars, nextCommand.variable);
            const returnObject = {
              variable: key,
              data: response,
            };
            localStore.commit('PLC_UPDATED', returnObject);
          } catch {
            this.commandCache.push(nextCommand);
          }
        }
      }
      setTimeout(() => {
        this.sendCommand(localStore);
      }, 100);
    }
    this.inSend = false;
  }

  public updatePLC(variable: string, value: string) {
    let plcVar = vars[variable];
    if (plcVar === undefined) {
      plcVar = variable;
    }
    this.sendMessage(commandType.UPDATE, plcVar, value.toString());
    return false;
  }

  public requestVariableUpdate(variable: string) {
    let plcVar = vars[variable];
    if (plcVar === undefined) {
      plcVar = variable;
    }
    this.sendMessage(commandType.GET, plcVar);
    return false;
  }

  public async receiveMessage(evt: MessageEvent, localStore: Store<RootState>) {
    const strLines = evt.data.split('\n');
    for (const line of strLines) {
      let returnedObject;
      if (line === '') {
        return;
      }
      try {
        returnedObject = JSON.parse(line);
      } catch (e) {
        // console.log(e + ': ' + evt.data)
        return; // error in the above string (in this case, yes)!
      }
      if (returnedObject.Command !== 0) {
        let keyname = getKeyByValue(vars, returnedObject.Variable);
        if (keyname === undefined) {
          keyname = returnedObject.Variable;
        }
        const variableToChange = {
          variable: keyname,
          data: JSON.parse(returnedObject.Value),
        };

        variableToChange.data = stringToBoolOrValue(variableToChange.data);
        localStore.commit('PLC_UPDATED', variableToChange);
        if (variableToChange.variable === 'status') {
          debugger;
        }
        return;
      }
      if (!this.store || this.pause) {
        return;
      }
      await this.store.dispatch('updateChartData', returnedObject.Value);
    }
  }

  public sendMessage(command: commandType, variable: string, value?: string) {
    const tempJson: PlcCommand = {
      command,
      variable,
      value: value || '',
    };
    this.commandCache.push(tempJson);
    if (this.inSend || !this.store) return;
    this.sendCommand(this.store);
  }

  public async zoomWindow(min: number, max: number, width: number = 400) {
    const grabUrl = `http://localhost:3000/eventgrabber?width=${width}&start=${min}&stop=${max}`;
    const resp: any = await ky.get(grabUrl).json();
    return resp.value;
  }

  public async getPoints(width?: number) {
    let grabUrl = '/eventgrabber';
    if (width) {
      grabUrl = `${grabUrl}?width=${width}`;
    }
    const resp: any = await ky.get(grabUrl).json();
    // if (!this.store || this.pause) {
    //   return;
    // }
    return resp;
  }
}
