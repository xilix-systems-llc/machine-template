export interface IHash {
  [key: string]: string;
}

export const vars: IHash = {
  alarms: 'GVL.AllAlarms',
  status: 'GVL.Status',
  plc: 'GVL.IO',
};

export default vars;
