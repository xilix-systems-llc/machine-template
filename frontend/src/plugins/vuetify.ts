import Vue from 'vue';
import '@mdi/font/css/materialdesignicons.css';
import 'roboto-fontface';
import Vuetify from 'vuetify/lib'; // TODO: use this instead once package is fixed
Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    dark: true,
  },
});
