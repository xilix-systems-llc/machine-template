module.exports = {
  configureWebpack: {
    devtool: 'source-map',
  },
  devServer: {
    proxy: {
      '/events': {
        target: 'http://localhost:5000/',
      },
      '/api/plc': {
        target: 'http://localhost:5000/',
      },
      '/eventgrabber': {
        target: 'http://localhost:5000/',
      },
      '/alarms': {
        target: 'http://localhost:5000/',
      },
    },
    clientLogLevel: 'info',
  },
  transpileDependencies: ['vuetify'],
};
