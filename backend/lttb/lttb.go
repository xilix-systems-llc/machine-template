package lttb // Point is a point on a line
import (
	"math"
)

type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}
type Points []Point

func (chartData Points) GetReducedPoints(numberOfPoints int) Points {
	reducedPoints := chartData.LTTB(numberOfPoints)
	return reducedPoints
}

func (points Points) GetZoomedPoints(start, end int, numberOfPoints int) Points {
	startIndexOfZoom, endIndexOfZoom := points.getStartAndEndIndex(float64(start), float64(end))
	reducedZoomSlice := points[startIndexOfZoom:endIndexOfZoom]
	reducedZoom := reducedZoomSlice.GetReducedPoints(numberOfPoints)
	returnPoints := make(Points, 0, 402)
	returnPoints = append(returnPoints, points[0])
	returnPoints = append(returnPoints, reducedZoom...)
	returnPoints = append(returnPoints, points[len(points)-1])
	return returnPoints
}

func (points Points) getStartAndEndIndex(start, end float64) (startIndex, endIndex int) {
	for index, point := range points {
		if point.X > start && startIndex == 0 {
			startIndex = index
		}
		if point.X > end {
			endIndex = index
			break
		}
	}
	return
}

// LTTB down-samples the data to contain only threshold number of points that
// have the same visual shape as the original data
func (data Points) LTTB(threshold int) Points {

	if threshold >= len(data) || threshold == 0 {
		return data // Nothing to do
	}

	sampled := make(Points, 0, threshold)

	// Bucket size. Leave room for start and end data points
	every := (data[len(data)-2].X - data[1].X) / float64(threshold)

	sampled = append(sampled, data[0]) // Always add the first point

	bucketStart := 1

	var lastBestPoint int
	for i := 0; i < threshold-2; i++ {
		if bucketStart >= len(data)-1 {
			break
		}
		bucketStartPos := (float64(i))*every + data[1].X
		bucketEndPos := (float64(i)+1)*every + data[1].X

		// Calculate point average for next bucket (containing c)
		avgRangeStart := bucketStartPos
		avgRangeEnd := bucketEndPos

		if avgRangeEnd >= data[len(data)-2].X {
			avgRangeEnd = data[len(data)-2].X
		}

		bucketIndex := bucketStart
		if avgRangeStart > avgRangeEnd {
			sampled = append(sampled, data[bucketIndex])
			bucketStart++
			break
		}

		var avgX, avgY float64
		var avgRangeLength int
		for ; avgRangeStart < avgRangeEnd; bucketIndex++ {
			avgX += data[bucketIndex].X
			avgY += data[bucketIndex].Y
			avgRangeStart = data[bucketIndex].X
			avgRangeLength++
		}
		avgX /= float64(avgRangeLength)
		avgY /= float64(avgRangeLength)

		rangeOffs := bucketStart
		rangeTo := bucketIndex

		// Point a
		pointAX := data[lastBestPoint].X
		pointAY := data[lastBestPoint].Y

		maxArea := -1.0

		skipIndex := int(math.Max(math.Floor(float64(avgRangeLength)/10.0), 1))
		var nextBestPoint int
		for ; rangeOffs < rangeTo; rangeOffs += skipIndex {
			// Calculate triangle area over three buckets
			area := (pointAX-avgX)*(data[rangeOffs].Y-pointAY) - (pointAX-data[rangeOffs].X)*(avgY-pointAY)
			// We only care about the relative area here.
			// Calling math.Abs() is slower than squaring
			area *= area
			if area > maxArea {
				maxArea = area
				nextBestPoint = rangeOffs // Next a is this b
			}
		}

		sampled = append(sampled, data[nextBestPoint]) // Pick this point from the bucket
		lastBestPoint = nextBestPoint                  // This a is the next a (chosen b)

		bucketStart = bucketIndex
	}

	sampled = append(sampled, data[len(data)-1]) // Always add last
	// fmt.Printf("number of reduced samples: %v\n", len(sampled))
	return sampled
}
