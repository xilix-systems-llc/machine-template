package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/xilix-systems-llc/machine-template/backend/broker"
	"gitlab.com/xilix-systems-llc/machine-template/backend/handlers"
	"gitlab.com/xilix-systems-llc/machine-template/backend/plc"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"

	"gitlab.com/xilix-systems-llc/go-native-ads/ads"
)

func fileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

func main() {

	ctx, cancel := context.WithCancel(context.Background())
	connection, err := ads.AddRemoteConnection(ctx, "5.15.131.166.1.1", 851)
	plc.InitPLC(connection)

	if err != nil {
		log.Fatalf("unable to make connection to plc %v", err)
	}
	r := chi.NewRouter()
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)
	r.Use(middleware.Logger)

	broker := broker.NewServer(ctx)
	h := handlers.NewHandler(ctx,
		connection,
		broker,
		"GVL.Status",
		"GVL.IO",
		"MAIN.i",
		"GVL.AllAlarms",
	)
	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "static")
	fileServer(r, "/", http.Dir(filesDir))

	r.HandleFunc("/events", broker.ServeHTTP)
	r.Get("/eventgrabber", h.GetZoomedValues)
	r.Route("/api/plc", func(r chi.Router) {
		r.Route("/{variable}", func(r chi.Router) {
			r.Get("/", h.GetVariable)
			r.Put("/", h.PostVariable)
		})
	})
	r.Get("/alarms", h.GetHistory)
	r.Get("/alarms/offset/{offset}", h.GetHistoryWithOffset)
	srv := &http.Server{
		Addr:    ":" + strconv.Itoa(5000),
		Handler: r,
	}
	r.Get("/alarms/count", h.GetNumberOfAlarmRecords)
	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	plc.LoadProgram("jobs/reset.star")

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	<-quit
	log.Println("Shutdown Server ...")
	cancel()
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		ads.Shutdown()
		log.Fatal("Server Shutdown:", err)
	}
	fmt.Println("closed")
	ads.Shutdown()
}
