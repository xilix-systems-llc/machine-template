package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"context"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/xilix-systems-llc/go-native-ads/ads"
	"gitlab.com/xilix-systems-llc/machine-template/backend/history"
	"gitlab.com/xilix-systems-llc/machine-template/backend/lttb"
)

func (h *Handler) GetVariable(w http.ResponseWriter, r *http.Request) {
	variable := chi.URLParam(r, "variable")
	if h.connection != nil {
		value, err := h.connection.Read(variable)
		if err != nil {
			fmt.Printf("error: %w", err)
			return
		}
		render.PlainText(w, r, value)
	} else {
		render.JSON(w, r, h.localVars[variable])
	}
	return
}

func (h *Handler) PostVariable(w http.ResponseWriter, r *http.Request) {
	variable := chi.URLParam(r, "variable")
	resp, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if h.connection != nil && err == nil {
		h.connection.Write(variable, string(resp))
	} else {
		h.localVars[variable] = string(resp)
	}
	return
}

func (h *Handler) notification(ctx context.Context, notification <-chan ads.NotificationStruct, broker chan []byte) {
ForLoop:
	for {
		select {
		case msg := <-notification:
			localJSON, err := json.Marshal(msg)
			if err == nil {
				if msg.Variable == "GVL.AllAlarms" {
					history.AlarmNotification <- msg.Value
				}
				select {
				case h.broker.Notifier <- localJSON:
					if msg.Variable == "MAIN.i" {
						// next := time.Now()
						nowMilli := time.Now().UnixNano() / int64(time.Millisecond)
						stringValue := strings.Replace(msg.Value, "\"", "", -1)
						receivedValue, err := strconv.ParseFloat(stringValue, 64)
						if err == nil {
							point := lttb.Point{X: float64(nowMilli), Y: receivedValue}
							h.points = append(h.points, point)
						}
						reducedPoints := h.points.GetReducedPoints(400)
						msg := InterfaceComm{
							Command:  UpdateGraph,
							Variable: "test",
							Value:    reducedPoints,
						}
						msgJSON, _ := json.Marshal(msg)
						h.broker.Notifier <- msgJSON
					}
				default:
				}
			}
		case <-ctx.Done():
			fmt.Println("done with notifications")
			break ForLoop
		}
	}
}
