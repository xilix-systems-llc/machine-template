package handlers

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/xilix-systems-llc/machine-template/backend/history"
)

func (h *Handler) GetHistory(w http.ResponseWriter, r *http.Request) {
	records := history.GetLatestRecords(0)
	render.JSON(w, r, records)
}

func (h *Handler) GetHistoryWithOffset(w http.ResponseWriter, r *http.Request) {
	variable := chi.URLParam(r, "offset")
	offset, err := strconv.Atoi(variable)
	if err != nil {
		return
	}
	records := history.GetLatestRecords(offset)
	render.JSON(w, r, records)
}

func (h *Handler) GetNumberOfAlarmRecords(w http.ResponseWriter, r *http.Request) {
	recordCount := history.GetRecordCount()
	render.JSON(w, r, recordCount)
}
