package handlers

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/render"
	"gitlab.com/xilix-systems-llc/go-native-ads/ads"
	"gitlab.com/xilix-systems-llc/machine-template/backend/broker"
	"gitlab.com/xilix-systems-llc/machine-template/backend/lttb"
)

type InterfaceCommand int

type InterfaceComm struct {
	Command  InterfaceCommand
	Variable string
	Value    lttb.Points
}

const (
	// UpdateGraph gets and returns a new graph
	UpdateGraph InterfaceCommand = iota
	// SetPLC Does stuff
	SetPLC
	// GetPLC More
	GetPLC
)

type msg struct {
	Command  string `json:"command"`
	Variable string `json:"variable"`
	Value    string `json:"value"`
}

func (h *Handler) getZoomedPoints(start int, stop int, numberOfPoints int) lttb.Points {
	reducedPoints := lttb.Points{}
	if start != 0 || stop != 0 {
		reducedPoints = h.points.GetReducedPoints(numberOfPoints)
	} else {
		reducedPoints = h.points.GetZoomedPoints(start, stop, numberOfPoints)
	}
	return reducedPoints
}

func (h *Handler) GetZoomedValues(rw http.ResponseWriter, rq *http.Request) {
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	start, startOk := rq.URL.Query()["start"]
	stop, stopOk := rq.URL.Query()["stop"]
	width, widthOk := rq.URL.Query()["width"]
	reducedPoints := lttb.Points{}
	numberOfPoints := 400
	if widthOk {
		parsedWidth, err := strconv.ParseInt(width[0], 10, 64)
		if err == nil {
			numberOfPoints = int(parsedWidth / 4.0)
		}
	}
	if !startOk || !stopOk {
		reducedPoints = h.points.GetReducedPoints(numberOfPoints)
	} else {
		startint, _ := strconv.ParseFloat(start[0], 64)
		endint, _ := strconv.ParseFloat(stop[0], 64)
		reducedPoints = h.getZoomedPoints(int(startint), int(endint), numberOfPoints)
	}
	msg := InterfaceComm{
		Command:  UpdateGraph,
		Variable: "test",
		Value:    reducedPoints,
	}
	render.JSON(rw, rq, msg)
}

func NewHandler(ctx context.Context, connection *ads.Connection, broker *broker.Broker, eventVars ...string) *Handler {
	h := &Handler{
		connection: connection,
		broker:     broker,
		localVars:  make(map[string]string),
		points:     lttb.Points{},
		eventVars:  eventVars,
	}

	go h.notification(ctx, connection.Update, broker.Notifier)
	for _, plcVar := range eventVars {
		connection.AddNotification(plcVar, ads.ADSTRANS_SERVERONCHA, 35*time.Millisecond, 75*time.Millisecond)
	}
	return h
}

type Handler struct {
	connection *ads.Connection
	broker     *broker.Broker
	localVars  map[string]string
	eventVars  []string
	points     lttb.Points
}
