package broker

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

func NewServer(ctx context.Context) (broker *Broker) {
	// Instantiate a broker
	broker = &Broker{
		ctx:            ctx,
		Notifier:       make(chan []byte, 1),
		newClients:     make(chan chan []byte),
		closingClients: make(chan chan []byte),
		clients:        make(map[chan []byte]bool),
	}

	// Set it running - listening and broadcasting events
	go broker.listen(ctx)

	return
}

type Broker struct {
	ctx context.Context

	// Events are pushed to this channel by the main events-gathering routine
	Notifier chan []byte

	// New client connections
	newClients chan chan []byte

	// Closed client connections
	closingClients chan chan []byte

	// Client connections registry
	clients map[chan []byte]bool
}

func (broker *Broker) ServeHTTP(rw http.ResponseWriter, req *http.Request) {

	// Make sure that the writer supports flushing.
	//
	flusher, ok := rw.(http.Flusher)

	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "text/event-stream; charset=utf-8")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	// Each connection registers its own message channel with the Broker's connections registry
	messageChan := make(chan []byte)

	// Signal the broker that we have a new connection
	broker.newClients <- messageChan

	for {
		select {
		case <-req.Context().Done():
			broker.closingClients <- messageChan
			return
		case <-broker.ctx.Done():
			broker.closingClients <- messageChan
			return
		case message := <-messageChan:
			fmt.Fprintf(rw, "data: %s\n\n", message)
			flusher.Flush()
		}
	}
}

func (broker *Broker) listen(ctx context.Context) {
	for {
		select {
		case s := <-broker.newClients:
			// A new client has connected.
			// Register their message channel
			broker.clients[s] = true
			log.Printf("Client added. %d registered clients", len(broker.clients))
		case s := <-broker.closingClients:
			// A client has dettached and we want to
			// stop sending them messages.
			delete(broker.clients, s)
			log.Printf("Removed client. %d registered clients", len(broker.clients))
		case event := <-broker.Notifier:
			// We got a new event from the outside!
			// Send event to all connected clients
			for clientMessageChan := range broker.clients {
				go func(c chan []byte) {
					select {
					case c <- event:
					default:
					}
				}(clientMessageChan)
			}
		}
	}
}
