package history

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"path"
	"regexp"
	"strconv"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"

	_ "github.com/mattn/go-sqlite3" // sqlite 3 driver
)

type alarmState int

const (
	INACTIVE alarmState = iota
	ACTIVE
)

type alarmRecord struct {
	AlarmId   int        `db:"AlarmId"`
	OccuredAt int64      `db:"OccuredAt"`
	State     alarmState `db:"State"`
}

type AlarmHMIRecord struct {
	AlarmName string      `db:"Name"`
	OccuredAt dbTimeStamp `db:"OccuredAt"`
	Id        int64       `db:"AlarmRecordId"`
	State     alarmState  `db:"State"`
}

// AlarmNotification is the channel to send alarm updates to
var AlarmNotification chan string
var localDb *sqlx.DB
var writeLock sync.Mutex

type allAlarms map[string]plcAlarm
type plcAlarm struct {
	Acknowledge convertibleBoolean
	Active      convertibleBoolean
	Name        string
	Number      int
}

type convertibleBoolean bool

func (bit *convertibleBoolean) UnmarshalJSON(data []byte) error {
	asString := string(data[1 : len(data)-1])
	boolValue, err := strconv.ParseBool(asString)
	if boolValue {
		*bit = true
	} else {
		*bit = false
	}
	if err != nil {
		return fmt.Errorf("Boolean unmarshal error: invalid input %s", asString)
	}
	return nil
}

type dbTimeStamp struct {
	time.Time
}

func (e *dbTimeStamp) Scan(src interface{}) error {
	var value int64
	switch src.(type) {
	case int64:
		value = src.(int64)
		dbTime := time.Unix(value, 0)
		*e = dbTimeStamp{Time: dbTime}
	default:
		return errors.New("Invalid type for dbTimeStamp")
	}
	return nil
}

func (time *dbTimeStamp) MarshalJSON() ([]byte, error) {
	jsonTime, _ := json.Marshal(time.Time)
	return jsonTime, nil
}

func init() {
	dbPath := path.Join("db", "history.db")
	db, err := sqlx.Connect("sqlite3", "file:"+dbPath+"?cache=shared")
	localDb = db
	if err != nil {
		log.Fatalln(err)
	}

	db.MustExec(createAlarmsSQL)
	db.MustExec(createAlarmRecordsSQL)

	AlarmNotification = make(chan string, 100)

	go func(notification <-chan string) {
		for {
			select {
			case alarm := <-notification:
				dealWithAlarm(alarm)
				break
			case <-time.After(100 * time.Millisecond):
				// fmt.Println("here")
				fakeAlarm := alarmRecord{AlarmId: 1, State: INACTIVE, OccuredAt: time.Now().Unix()}
				fakeAlarm.writeToDb()
				break
			}
		}
	}(AlarmNotification)
}

func dealWithAlarm(alarm string) {
	getNumberRegex := regexp.MustCompile(`\[(\d*)\]`)
	alarms := allAlarms{}
	json.Unmarshal([]byte(alarm), &alarms)
	for key, singleAlarm := range alarms {
		matched := getNumberRegex.FindStringSubmatch(key)
		alarmNumber, _ := strconv.Atoi(matched[1])
		if singleAlarm.Name != "" {
			updateAlarmName(singleAlarm.Name, alarmNumber)
		}
		alarmToLog := &alarmRecord{
			AlarmId:   alarmNumber,
			OccuredAt: time.Now().Unix(),
		}
		if singleAlarm.Active {
			alarmToLog.State = ACTIVE
		} else {
			alarmToLog.State = INACTIVE
		}
		alarmToLog.writeToDb()
	}
}

func updateAlarmName(name string, number int) {
	writeLock.Lock()
	defer writeLock.Unlock()
	fmt.Printf("%v %v\n", name, number)
	_, err := localDb.NamedExec(`
	INSERT INTO Alarms(Name, AlarmId) VALUES(:name, :number)
	ON CONFLICT(AlarmId) 
	DO UPDATE SET Name=excluded.Name;`,
		map[string]interface{}{
			"name":   name,
			"number": number,
		})
	if err != nil {
		fmt.Printf("eeks: %v", err)
	}
}

func (alarm *alarmRecord) writeToDb() {
	writeLock.Lock()
	defer writeLock.Unlock()
	lastRecord := &alarmRecord{}
	err := localDb.Get(lastRecord, `
	SELECT 
		AlarmRecords.State 
	FROM AlarmRecords
	WHERE AlarmRecords.AlarmId = ?
	ORDER BY AlarmRecords.AlarmRecordId DESC LIMIT 1
	`, alarm.AlarmId)
	if lastRecord.State == alarm.State {
		return
	}
	_, err = localDb.NamedExec(`
	INSERT INTO 
		AlarmRecords(AlarmId, OccuredAt, State) 
	VALUES(:AlarmId, :OccuredAt, :State)`, alarm)
	if err != nil {
		fmt.Println(err)
	}
}

func GetLatestRecords(offset int) []AlarmHMIRecord {
	writeLock.Lock()
	defer writeLock.Unlock()
	alarms := []AlarmHMIRecord{}
	err := localDb.Select(&alarms, `
	SELECT
		Alarms.Name,
		AlarmRecords.OccuredAt,
		AlarmRecords.AlarmRecordId,
		AlarmRecords.State
	FROM
		Alarms
	JOIN AlarmRecords USING(AlarmId)
	WHERE Alarms.Name IS NOT NULL 
	ORDER BY AlarmRecords.OccuredAt DESC, AlarmRecords.AlarmRecordId DESC
	LIMIT 10 OFFSET ?
	`, offset)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return alarms
}

func ClearAlarms() {
	writeLock.Lock()
	defer writeLock.Unlock()
	_, err := localDb.Exec(`
	DELETE from AlarmRecords
	`)
	if err != nil {
		fmt.Println(err)
	}
}

func GetRecordCount() int {
	writeLock.Lock()
	defer writeLock.Unlock()
	result := localDb.QueryRow(`
	SELECT
		COUNT(*)
	FROM
		Alarms
	JOIN AlarmRecords USING (AlarmId)
	WHERE Alarms.Name IS NOT NULL
`)
	rows := 0
	result.Scan(&rows)
	return rows
}
