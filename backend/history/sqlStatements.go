package history

const createAlarmRecordsSQL = `
CREATE TABLE IF NOT EXISTS AlarmRecords (
	AlarmRecordId 	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	AlarmId 		INTEGER, 
	OccuredAt 		INTEGER,
	State	 		INTEGER
)
`
const createAlarmsSQL = `
CREATE TABLE IF NOT EXISTS Alarms (
	AlarmId INTEGER PRIMARY KEY UNIQUE,
	Name 	TEXT
)
`

const createIndexesSQL = `
CREATE UNIQUE INDEX AlarmRecords_OccuredAt_Index ON AlarmRecords ( OccuredAt )
CREATE UNIQUE INDEX Alarms_AlarmId_Index ON Alarms ( AlarmId )
`
