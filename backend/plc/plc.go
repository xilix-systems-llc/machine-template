package plc

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"gitlab.com/xilix-systems-llc/go-native-ads/ads"
	"go.starlark.net/starlark"
)

type PLCCommand int

type plcIO struct {
	execute     *ads.Symbol
	done        *ads.Symbol
	nextCommand *ads.Symbol
}

var plc *plcIO

func InitPLC(connection *ads.Connection) {
	plc = &plcIO{}
	plc.done, _ = connection.GetSymbol("Interpreter.Done")
	plc.execute, _ = connection.GetSymbol("Interpreter.Execute")
	plc.nextCommand, _ = connection.GetSymbol("Interpreter.NextCommand")
}

func waitForComplete() {
	plc.execute.Write("true")
	for {
		resp, _ := plc.done.Read()
		var str string
		json.Unmarshal([]byte(resp), &str)
		if str == "true" {
			fmt.Println(str)
			break
		}
	}
	plc.execute.Write("false")
	for {
		resp, _ := plc.done.Read()
		var str string
		json.Unmarshal([]byte(resp), &str)
		if str == "false" {
			fmt.Println(str)
			break
		}
	}
}

func dwell(thread *starlark.Thread, b *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	n := 1
	if err := starlark.UnpackArgs(b.Name(), args, kwargs, "n", &n); err != nil {
		return nil, err
	}
	time.Sleep(time.Duration(n) * time.Millisecond)
	return starlark.None, nil
}

func testInterpreter(thread *starlark.Thread, b *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	plc.nextCommand.Write(strconv.Itoa(int(TestInterpreter)))
	waitForComplete()
	return starlark.None, nil
}

func LoadProgram(filename string) {
	thread := &starlark.Thread{
		Name:  "example",
		Print: func(_ *starlark.Thread, msg string) { fmt.Println(msg) },
	}
	predeclared := starlark.StringDict{
		"dwell":          starlark.NewBuiltin("dwell", dwell),
		"test_intpreter": starlark.NewBuiltin("testInterpreter", testInterpreter),
	}
	globals, err := starlark.ExecFile(thread, filename, nil, predeclared)

	if err != nil {
		if evalErr, ok := err.(*starlark.EvalError); ok {
			fmt.Printf("errrrror \n")
			log.Fatal(evalErr.Backtrace())
		}
		fmt.Printf("we've hit an error: %v", err)
	}

	// Print the global environment.
	fmt.Println("\nGlobals:")
	for _, name := range globals.Keys() {
		v := globals[name]
		fmt.Printf("%s (%s) = %s\n", name, v.Type(), v.String())
	}
}
